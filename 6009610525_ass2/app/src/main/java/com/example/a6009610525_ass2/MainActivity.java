package com.example.a6009610525_ass2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    SensorManager sensorManager;
    private static final int POLL_INTERVAL = 500;
    private static final int INTERVAL_MS = 20;
    private Handler hdr = new Handler();
    SensorInfo sensor_info = new SensorInfo();
    SensorInfo previous_sensor_info = new SensorInfo();
    private static final int shake_threshold = 10;
    private int shake_count = 0;
    private long previousTimestamp;
    Boolean shown_dialog = false;

    private Runnable pollTask = new Runnable() {
        public void run() {
            showDialog();
            hdr.postDelayed(pollTask, POLL_INTERVAL);
        }
    };

    @SuppressLint("InvalidWakeLockTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
        ImageView img = (ImageView) findViewById(R.id.imageView);
        BitmapDrawable[] frame = new BitmapDrawable[9];
        int i=0;
        for(i=1; i<=8; i++){
            frame[i] = (BitmapDrawable)getResources().getDrawable(

                    getResources().getIdentifier((String)"ss"+i, "drawable", this.getPackageName()) );
        }
        int reasonableDuration = 200;
        AnimationDrawable mAnimation = new AnimationDrawable();
        for(i=1; i<=8; i++){
            mAnimation.addFrame(frame[i], reasonableDuration);
        }
        img.setImageDrawable(mAnimation);
        mAnimation.start();
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy){
        // TO DO
    }

    public void onSensorChanged(SensorEvent event){
        int type = event.sensor.getType();
        if (type == Sensor.TYPE_ACCELEROMETER) {
            sensor_info.accX=event.values[0];
            sensor_info.accY=event.values[1];
            sensor_info.accZ=event.values[2];
        }
    }

    public void showDialog() {
        Random rand = new Random();
        if( (Math.abs(sensor_info.accX-previous_sensor_info.accX)>shake_threshold) || (Math.abs(sensor_info.accY-previous_sensor_info.accY)>shake_threshold) || (Math.abs(sensor_info.accZ-previous_sensor_info.accZ)>shake_threshold) ) {
            final long now = System.currentTimeMillis();
            if (now - previousTimestamp > INTERVAL_MS) {
                shake_count++;
                if(shake_count>=2){
                    if(!shown_dialog) {
                        onPause();
                        shown_dialog = true;
                        int rand_int1 = rand.nextInt(28)+1;
                        final AlertDialog.Builder viewDialog = new AlertDialog.Builder(this);
                        viewDialog.setIcon(R.drawable.seamsee);
                        switch(rand_int1) {
                            case 1:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_1)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_1_dt)));
                                break;
                            case 2:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_2)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_2_dt)));
                                break;
                            case 3:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_3)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_3_dt)));
                                break;
                            case 4:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_4)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_4_dt)));
                                break;
                            case 5:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_5)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_5_dt)));
                                break;
                            case 6:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_6)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_6_dt)));
                                break;
                            case 7:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_7)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_7_dt)));
                                break;
                            case 8:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_8)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_8_dt)));
                                break;
                            case 9:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_9)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_9_dt)));
                                break;
                            case 10:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_10)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_10_dt)));
                                break;
                            case 11:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_11)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_11_dt)));
                                break;
                            case 12:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_12)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_12_dt)));
                                break;
                            case 13:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_13)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_13_dt)));
                                break;
                            case 14:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_14)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_14_dt)));
                                break;
                            case 15:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_15)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_15_dt)));
                                break;
                            case 16:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_16)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_16_dt)));
                                break;
                            case 17:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_17)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_17_dt)));
                                break;
                            case 18:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_18)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_18_dt)));
                                break;
                            case 19:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_19)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_19_dt)));
                                break;
                            case 20:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_20)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_20_dt)));
                                break;
                            case 21:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_21)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_21_dt)));
                                break;
                            case 22:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_22)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_22_dt)));
                                break;
                            case 23:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_23)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_23_dt)));
                                break;
                            case 24:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_24)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_24_dt)));
                                break;
                            case 25:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_25)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_25_dt)));
                                break;
                            case 26:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_26)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_26_dt)));
                                break;
                            case 27:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_27)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_27_dt)));
                                break;
                            case 28:
                                viewDialog.setTitle(String.format(getResources().getString(R.string.ss_28)));
                                viewDialog.setMessage(String.format(getResources().getString(R.string.ss_28_dt)));
                                break;

                            default:
                        }
                        viewDialog.setPositiveButton("Close", null);
                        viewDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                shake_count=0;
                                onResume();
                                shown_dialog = false;
                            }
                        });
                        viewDialog.show();
                    }
                }

                previousTimestamp = now;
            }
            previous_sensor_info.accX = sensor_info.accX;
            previous_sensor_info.accY = sensor_info.accY;
            previous_sensor_info.accZ = sensor_info.accZ;
        }
    }

    @SuppressLint("WakelockTimeout")
    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        hdr.postDelayed(pollTask, POLL_INTERVAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
        hdr.removeCallbacks(pollTask);
    }

    static class SensorInfo{
        float accX, accY, accZ;
    }

}//end MainActivity